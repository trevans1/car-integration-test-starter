package org.launchcode.training;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.launchcode.training.data.CarMemoryRepository;
import org.launchcode.training.data.CarRepository;
import org.launchcode.training.models.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by LaunchCode
 */
@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class CarControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CarRepository carRepository;

    @Test
    public void viewCarById() throws Exception {
        Car car = new Car("Honda", "CR-V", 13, 26);
        carRepository.save(car);

        mockMvc.perform(get("/car/" + car.getId()))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Honda")));
    }

    @Test
    public void MultipleCarsShowOnIndex() throws Exception {
        Car car = new Car("Honda", "CR-V", 13, 26);
        carRepository.save(car);
        car = new Car("Toyota", "Civic", 12, 50);
        carRepository.save(car);

        mockMvc.perform(get("/car"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Honda")))
                .andExpect(content().string(containsString("CR-V")))
                .andExpect(content().string(containsString("Toyota")))
                .andExpect(content().string(containsString("Civic")));
    }

}
