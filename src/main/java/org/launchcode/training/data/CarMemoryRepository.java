package org.launchcode.training.data;


import org.launchcode.training.models.Car;

import java.util.ArrayList;
import java.util.List;

public class CarMemoryRepository {

    private static ArrayList<Car> carStorage = new ArrayList<>();

    public List<Car> findAll() {
        return new ArrayList<>(carStorage);
    }

    public void save(Car cart) {
        carStorage.add(cart);
    }

    public void clear() {
        carStorage.clear();
    }

}
