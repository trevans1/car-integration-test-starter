package org.launchcode.training.controllers;

import org.launchcode.training.data.CarMemoryRepository;
import org.launchcode.training.models.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by LaunchCode
 */
@Controller
@RequestMapping("car")
public class CarController {
    @Autowired
    private org.launchcode.training.data.CarRepository carRepository;

    @RequestMapping(value ="/seedData")
    public String seedDatabase() {
        // Go to /car/seedData in browser to add some cars to your app
        Car prius = new Car("Toyota", "Prius", 10, 50);
        Car jeep = new Car("Jeep", "Grand Cherokee ", 20, 25);
        Car jetta = new Car("Volkswagen", "Jetta", 12, 35);
        carRepository.save(prius);
        carRepository.save(jeep);
        carRepository.save(jetta);
        return "car/seeded";
    }

    @RequestMapping(value="/{id}")
    public String showCarIdPage(Model t, @PathVariable int id){
        Car car;
        try {
            car = carRepository.findAll().stream()
                    .filter(c -> c.getId() == id)
                    .collect(Collectors.toList()).get(0);
        }
        catch (Exception e){
            return "car/index";
        }

        t.addAttribute("car", car);
        return "car/view";
    }

    @RequestMapping
    public String index(Model model){
        List<Car> cars = carRepository.findAll();
        model.addAttribute("cars", cars);
        return "car/index";
    }

}
